# Lab 9 Solution

Website: <https://juice-shop.herokuapp.com/#/>

## 1. Search

| Test Step                              | Result                                                                                                                       |
| -------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------- |
| Click Search                           | OK                                                                                                                           |
| Search for `juice`                     | OK                                                                                                                           |
| Search for `' OR 1=1 --`               | OK, no results                                                                                                               |
| Search for `<script>alert(1)</script>` | The script is inserted in the page, but the alert is cancelled by the browser <https://owasp.org/www-community/attacks/xss/> |

## 2. Registration

| Test Step                                       | Result                            |
| ----------------------------------------------- | --------------------------------- |
| Register with invalid email                     | The frontend filters the email    |
| Register with unavailable email                 | OK                                |
| Register password not matching the requirements | The frontend filters the password |

## 3. Login

| Test Step                                                   | Result                                                                          |
| ----------------------------------------------------------- | ------------------------------------------------------------------------------- |
| Login with invalid credentials                              | Login rejected                                                                  |
| Login with valid credentials                                | OK                                                                              |
| Login with creds login=`' OR 1=1 --` password=`' OR 1=1 --` | OK, Logged in as admin, <https://owasp.org/www-community/attacks/SQL_Injection> |

## 4. Admin page

The page: <https://juice-shop.herokuapp.com/#/administration> can be found via url guessing or brute-forcing.

| Test Step                                                     | Result            |
| ------------------------------------------------------------- | ----------------- |
| Go to the admin page without account                          | 403 access denied |
| Go to the admin page with an non-admin account                | 403 access denied |
| Go to the admin page with an admin account from previous step | OK                |
